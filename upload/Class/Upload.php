<?php
/**
 * @copyright (c) Rafael da Silva Lima WebStylus
 * http://www.webstylus.com.br
 * 22/janeiro/2018
 */

namespace Upload;

use Url\Url;

class Upload
{
    private $Arquivo;
    private $Titulo;

    private $Largura;
    private $Tamanho;
    private $Imagem;

    private $Retorno;
    private $Erro;

    private $Disco;

    private static $Pasta;
    private static $Destino;
    private static $BaseDir;

    protected static $MimeTypes = [
        'arquivo' => [
            'application/pdf',
            'application/excel',
            "application/octet-stream",
            "application/vnd.ms-excel",
            "application/x-csv",
            "application/csv",
            'text/csv',
            'application/msword',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'binary/octet-stream',
        ],
        'media' => [
            'audio/mp3',
            'audio/mpeg',
            'video/mp4',
            'video/3gpp',
            'video/flv',
            'video/x-flv',
        ],
    ];

    /**
     * Upload constructor.
     * @param null $BaseDir = Pode receber os diretórrios separados por / ou DIRECTORY_SEPARATOR
     * Exemplos:
     *  1 = imagens/2018
     *  2 = imagens . DIRECTORY_SEPARATOR . 2018
     *  3 = imagens
     * Se não for declarado um diretório, a pasta tmp, será criada na raiz de execução da lib
     * Trabalhar sempre com caminhos absolutos para os diretórios.
     */
    public function __construct($BaseDir = null)
    {
        self::$BaseDir = ($BaseDir ? $BaseDir : 'tmp');
        self::$BaseDir = Url::amigavel(self::$BaseDir);

        self::criaDiretorio(self::$BaseDir);
    }

    /**
     * Verifica a quantidade em disco utilizada pela pasta setada em instanciar o método
     * @return mixed = retorna int ou float
     */
    public function espacoEmDiscoUsado()
    {
        $path = realpath(self::$BaseDir);
        if ($path !== false) {
            foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path, \FilesystemIterator::SKIP_DOTS)) as $object) {
                $this->Disco += $object->getSize();
            }
        }
        return floor($this->Disco / (1024*1024)) . 'Mb';
    }

    /**
     * Trata o envelope das informações e define sua rota de envio, após isto envia o arquivo para o destino
     * @param array $imagem = Recebe o $_FILES[]
     * @param null $titulo = Recebe o titulo que a imagem vai levar
     * @param null $largura = Se não definido recebe 1024 por padrão
     * @param null $pasta = SE não definido será enviada para /imagens por padrão
     */
    public function enviarImagem(array $imagem, $titulo = null, $largura = null, $pasta = null)
    {
        $this->Arquivo = $imagem;
        $this->Titulo = ((string)$titulo ? $titulo : substr($imagem['name'], 0, strrpos($imagem['name'], '.')));
        $this->Largura = ((int)$largura ? $largura : 1024);

        self::$Pasta = ($pasta ? $pasta : 'imagens');
        self::$Pasta = Url::amigavel(self::$Pasta);

        self::verificaDiretorio();
        $this->defineNomeDoArquivo();
        $this->uploadImage();
    }

    public function enviarArquivo(array $arquivo, $titulo = null, $pasta = null, $tamanho = null)
    {
        $this->Arquivo = $arquivo;
        $this->Titulo = ((string)$titulo ? $titulo : substr($this->Arquivo['name'], 0, strrpos($this->Arquivo['name'], '.')));
        $this->Tamanho = ((int)$tamanho ? $tamanho : 2);

        self::$Pasta = ($pasta ? $pasta : 'arquivos');
        self::$Pasta = Url::amigavel(self::$Pasta);

        $this->uploadFile();
    }

    public function enviarMedia(array $media, $titulo = null, $pasta = null, $tamanho = null)
    {
        $this->Arquivo = $media;
        $this->Titulo = ((string)$titulo ? $titulo : substr($this->Arquivo['name'], 0, strrpos($this->Arquivo['name'], '.')));
        $this->Tamanho = ((int)$tamanho ? $tamanho : 40);

        self::$Pasta = ((string)$pasta ? $pasta : 'medias');

        $this->uploadMedia();
    }

    /**
     * @return mixed
     *
     */
    public function getRetorno()
    {
        return $this->Retorno;
    }

    /**
     * @return mixed
     */
    public function getErro()
    {
        return $this->Erro;
    }

    /**
     * Método que gera o nome da imagem que será enviada para o diretório
     * Esta imagem terá o titulo montado com o de uma url amigável
     * Exemplo: imagem-enviada.jpg se orquivo já existir no diretório será usado uma hash, algo parecido com:
     * imagem-enviada-ac2r45be.jpg assim evitando que uma imagem sobreponha a outra.
     */
    private function defineNomeDoArquivo()
    {
        $Titulo = Url::amigavel($this->Titulo) . strrchr($this->Arquivo['name'], '.');
        if (file_exists(self::$Destino . DIRECTORY_SEPARATOR . $Titulo)):
            $Titulo = Url::amigavel($this->Titulo) . '-' . substr(md5(time()), 0, 5) . strrchr($this->Arquivo['name'], '.');
        endif;
        $this->Titulo = $Titulo;
    }

    /**
     * @param $pasta
     * Cria a pasta caso ela não exista na aplicação, seja ela declarada no método ou setada por padrão.
     */
    private static function criaDiretorio($pasta)
    {
        if (!file_exists($pasta) && !is_dir($pasta)) {
            $pasta = explode(DIRECTORY_SEPARATOR, trim($pasta, DIRECTORY_SEPARATOR));
            $recursivo = null;
            foreach ($pasta as $chave => $valor) {
                $recursivo .= $valor . DIRECTORY_SEPARATOR;
                if (!file_exists($recursivo) && !is_dir($recursivo)) {
                    if (mkdir($recursivo, 0777, true)) {
                        chmod($recursivo, 0777);
                    }
                }
            }
        }
    }

    /**
     * @param $pasta = Pasta característica do item, imagens, arquivos, videos, midias, downloads etc...
     * Solicita a criação do diretório Ano e Mês dentro da $pasta enviada.
     * Exemplo: tmp/2018/01 ou até mesmo tmp/imagens/2018/01
     * Onde $pasta pode ser tmp/imagens ou até mesmo tmp/imagens/png
     */
    private static function verificaDiretorio()
    {
        list($y, $m) = explode('/', date('Y/m'));
        self::$Destino = self::$BaseDir . DIRECTORY_SEPARATOR . self::$Pasta . DIRECTORY_SEPARATOR . $y . DIRECTORY_SEPARATOR . $m;
        self::criaDiretorio(self::$Destino);
    }


    /**
     * Realiza o upload da imagem tratando ela com a library GD
     * Verificar se a mesma está instalada e habilitada
     */
    private function uploadImage()
    {
        $type = getimagesize($this->Arquivo['tmp_name']);
        $type = $type['mime'];

        switch ($type):
            case 'image/jpg':
            case 'image/jpeg':
            case 'image/pjpeg':
                $this->Imagem = imagecreatefromjpeg($this->Arquivo['tmp_name']);
                break;
            case 'image/png':
            case 'image/x-png':
                $this->Imagem = imagecreatefrompng($this->Arquivo['tmp_name']);
                break;
        endswitch;

        if (!$this->Imagem):
            $this->Retorno = false;
            $this->Erro = 'Tipo de arquivo inválido, envie imagens JPG ou PNG.';
        else:
            $x = imagesx($this->Imagem);
            $y = imagesy($this->Imagem);
            $ImageX = ($this->Largura < $x ? $this->Largura : $x);
            $ImageH = ($ImageX * $y) / $x;

            $NewImage = imagecreatetruecolor($ImageX, $ImageH);
            imagealphablending($NewImage, false);
            imagesavealpha($NewImage, true);
            imagecopyresampled($NewImage, $this->Imagem, 0, 0, 0, 0, $ImageX, $ImageH, $x, $y);

            switch ($type):
                case 'image/jpg':
                case 'image/jpeg':
                case 'image/pjpeg':
                    imagejpeg($NewImage, self::$Destino . DIRECTORY_SEPARATOR . $this->Titulo);
                    break;
                case 'image/png':
                case 'image/x-png':
                    imagepng($NewImage, self::$Destino . DIRECTORY_SEPARATOR . $this->Titulo);
                    break;
            endswitch;

            if (!$NewImage):
                $this->Erro = 'Não foi possível gerar a nova imagem para envio, verifique o arquivo.';
                $this->Retorno = false;
            else:
                $this->Retorno = [
                    'diretorio' => self::$Destino . DIRECTORY_SEPARATOR,
                    'arquivo' => $this->Titulo,
                    'url' => $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . self::$Destino . DIRECTORY_SEPARATOR . $this->Titulo,
                    'fisico' => self::$Destino . DIRECTORY_SEPARATOR . $this->Titulo,
                ];
                chmod($this->Retorno['fisico'], 0777);
            endif;

            imagedestroy($this->Imagem);
            imagedestroy($NewImage);
        endif;
    }

    public function uploadFile()
    {
        if ($this->Arquivo['size'] > ($this->Tamanho * (1024 * 1024))):
            $this->Retorno = false;
            $this->Erro = "Arquivo muito grande, tamanho máximo permitido de {$this->Tamanho}mb.";
        elseif (!in_array($this->Arquivo['type'], self::$MimeTypes['arquivo'])):
            $this->Retorno = false;
            $this->Erro = 'Tipo de arquivo não suportado.';
        else:
            self::verificaDiretorio();
            $this->defineNomeDoArquivo();
            $this->MoverArquivo();
        endif;
    }

    private function uploadMedia()
    {
        if ($this->Arquivo['size'] > ($this->Tamanho * (1024 * 1024))):
            $this->Retorno = false;
            $this->Erro = "Arquivo muito grande, tamanho máximo permitido de {$this->Tamanho}mb.";
        elseif (!in_array($this->Arquivo['type'], self::$MimeTypes['media'])):
            $this->Retorno = false;
            $this->Erro = 'Tipo de arquivo não suportado.';
        else:
            self::verificaDiretorio();
            $this->defineNomeDoArquivo();
            $this->MoverArquivo();
        endif;
    }

    private function MoverArquivo()
    {
        if (!move_uploaded_file($this->Arquivo['tmp_name'], self::$Destino . DIRECTORY_SEPARATOR . $this->Titulo)):
            $this->Retorno = false;
            $this->Erro = 'Erro ao mover o arquivo. Por favor tente mais tarde.';
        else:
            $this->Retorno = [
                'diretorio' => self::$Destino . DIRECTORY_SEPARATOR,
                'arquivo' => $this->Titulo,
                'url' => $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . self::$Destino . DIRECTORY_SEPARATOR . $this->Titulo,
                'fisico' => self::$Destino . DIRECTORY_SEPARATOR . $this->Titulo,
            ];
            chmod($this->Retorno['fisico'], 0777);
        endif;
    }

}