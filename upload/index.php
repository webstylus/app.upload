<?php require __DIR__ . DIRECTORY_SEPARATOR . "lib/autoload.php"; ?>
<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8"/>
    <link href="favicon.ico" rel="shortcut icon"/>
    <title>Upload Files</title>
</head>

<style>
    form label{
        display: block;
    }
    form label span{
        display: block;
        margin: 1% auto;
        text-transform: uppercase;
        padding: 1%;
        background: #eee;
        border: 1px solid #ccc;
    }
    form input[type='submit']{
        font-size: 1.2em;
    }
</style>
<body>
<div class="code">
    <?php
    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    $upload = new Upload\Upload();

    if (isset($post['enviarI']) && $_FILES['imagens']['error'] != 4):
        $upload->enviarImagem($_FILES['imagens']);
        unset($post, $_FILES);
        if (!$upload->getRetorno()):
            echo $upload->getErro();
        else:
            include('arquivo.inc.php');
        endif;
    elseif (isset($post['enviarA']) && $_FILES['arquivos']['error'] != 4):
        $upload->enviarArquivo($_FILES['arquivos']);
        unset($post, $_FILES);
        if (!$upload->getRetorno()):
            echo $upload->getErro();
        else:
            include('arquivo.inc.php');
        endif;
    elseif (isset($post['enviarM']) && $_FILES['medias']['error'] != 4):
        $upload->enviarMedia($_FILES['medias']);
        unset($post, $_FILES);
        if (!$upload->getRetorno()):
            echo $upload->getErro();
        else:
            include('arquivo.inc.php');
        endif;
    endif;
    ?>
</div>

<h2>Disco utilizado: <?= $upload->espacoEmDiscoUsado(); ?></h2>

<hr/>
<br/>
<form method="post" enctype="multipart/form-data">
    <label>
        <span>Imagens</span>
        <input type="file" multiple name="imagens"/>
    </label>

    <br/>
    <br/>
    <input type="submit" name="enviarI" value="Enviar imagem"/>
</form>
<hr/>
<form method="post" enctype="multipart/form-data">
    <label>
        <span>Arquivos</span>
        <input type="file" multiple name="arquivos"/>
    </label>

    <br/>
    <br/>
    <input type="submit" name="enviarA" value="Enviar arquivo"/>
</form>
<hr/>
<form method="post" enctype="multipart/form-data">
    <label>
        <span>Medias</span>
        <input type="file" multiple name="medias"/>
    </label>

    <br/>
    <br/>
    <input type="submit" name="enviarM" value="Enviar media"/>
</form>
</body>
</html>
