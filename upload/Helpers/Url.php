<?php
/**
 * @copyright (c) Rafael da Silva Lima WebStylus
 * http://www.webstylus.com.br
 */

namespace Url;

abstract class Url
{
    protected static $Dados;
    protected static $Formato;

    public static function amigavel($titulo)
    {
        self::$Formato['a'] = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]|/?;:.,\\\'<>°ºª®';
        self::$Formato['b'] = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 ';

        self::$Dados['a'] = strtr(utf8_decode($titulo), utf8_decode(self::$Formato['a']), self::$Formato['b']);
        self::$Dados['b'] = strtr($titulo, self::$Formato['a'], self::$Formato['b']);

        if (mb_detect_encoding(self::$Dados['b'], 'UTF-8, ISO-8859-1, ASCII') == 'ISO-8859-1') {
            self::$Dados['c'] = strip_tags(trim($titulo));
            self::$Dados['c'] = str_replace(' ', '-', self::$Dados['c']);
            self::$Dados['c'] = str_replace(array('-----', '----', '---', '--'), '-', self::$Dados['c']);
        } else {
            self::$Dados['c'] = strip_tags(trim(self::$Dados['a']));
            self::$Dados['c'] = iconv("UTF-8", "ASCII//TRANSLIT//IGNORE", self::$Dados['c']);
            self::$Dados['c'] = str_replace(' ', '-', self::$Dados['c']);
            self::$Dados['c'] = utf8_decode(str_replace(array('-----', '----', '---', '--'), '-', self::$Dados['c']));
        }

        return strtolower(self::$Dados['c']);
    }
}